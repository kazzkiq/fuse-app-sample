var Utils = require("../Modules/Utils");

var data = {};

data.birthday = this.Parameter.map(function(param) {
    return param.birthday;
});

data.startDate = this.Parameter.map(function(param) {
    return Utils.formatDate(param.startDate);
});

data.endDate = this.Parameter.map(function(param) {
    return Utils.formatDate(param.endDate);
});

module.exports = {
    birthday: data.birthday,
    startDate: data.startDate,
    endDate: data.endDate
};