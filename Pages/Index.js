var Observable = require("FuseJS/Observable");
var Utils = require("../Modules/Utils");

var nascDay = Observable();
var nascMonth = Observable();
var nascYear = Observable();

nascDay.value = "";
nascMonth.value = "";
nascYear.value = "";

var calendar = {
	"01": "10/03/2017",
	"02": "10/03/2017",
	"03": "08/04/2017",
	"04": "08/04/2017",
	"05": "08/04/2017",
	"06": "12/05/2017",
	"07": "12/05/2017",
	"08": "12/05/2017",
	"09": "16/06/2017",
	"10": "16/06/2017",
	"11": "16/06/2017",
	"12": "14/07/2017",
}

var dataTermino = "31/07/2017";

function checkFGTS () {
	var currentDate = new Date();
	var data = {
		birthday: nascDay.value + "/" + nascMonth.value + "/" + nascYear.value,
		startDate: calendar[Utils.leftPad("" + nascMonth.value + "")],
		endDate: dataTermino
	}

	// Loose check to see if date is valid
	if(([nascDay.value, nascMonth.value, nascYear.value].join()).length < 4) {
		return;
	}
	if(nascDay.value > 31) {
		return;
	}
	if(nascMonth.value > 12 || nascMonth < 1) {
		return;
	}
	if(nascYear.value > 2010) {
		return;
	}

	// Send to proper View based on date
	if(nascMonth.value <= (+currentDate.getMonth() + 1)) {
		router.push("success", data);
	} else {
		router.push("soon", data);
	}
}

module.exports = {
	nascDay: nascDay,
	nascMonth: nascMonth,
	nascYear: nascYear,
	checkFGTS: checkFGTS
};