function gotoIndex () {
	router.goto("index")
}

function gotoSuccess () {
	router.goto("success")
}

function gotoSoon () {
	//router.goto("soon")
}

function gotoBack () {
	router.goBack();
}

module.exports = {
	gotoIndex: gotoIndex,
	gotoSuccess: gotoSuccess,
	gotoSoon: gotoSoon,
	gotoBack: gotoBack
};