var monthNames = [
	"Janeiro",
	"Fevereiro",
	"Março",
	"Abril",
	"Maio",
	"Junho",
	"Julho",
	"Agosto",
	"Setembro",
	"Outubro",
	"Novembro",
	"Dezembro"
];

function leftPad (month) {
	var pad = "00";
	return pad.substring(0, pad.length - month.length) + month;
}

function formatDate (date) {
	var oloco = date;
	if(!date || date == "" || date.length < 1) {
		return oloco;
	}

	var ymd = date.split("/").reverse().join("-");
	var d = new Date(ymd);
	return d.getUTCDate() + " de " + monthNames[d.getUTCMonth()];
}

module.exports = {
	leftPad: leftPad,
	formatDate: formatDate,
	monthNames: monthNames
}